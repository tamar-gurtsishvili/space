import { Component, TemplateRef } from '@angular/core';

import { AlertService } from '@core/services/alert.service';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss'],
  host: { '[class.ngb-toasts]': 'true' },
})
export class AlertComponent {
  constructor(public alertService: AlertService) {}

  isTemplate(toast) {
    return toast.textOrTpl instanceof TemplateRef;
  }
}
