import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  NgbDatepickerModule,
  NgbToastModule,
  NgbModalModule,
} from '@ng-bootstrap/ng-bootstrap';

import { AlertComponent } from './components/alert/alert.component';
import { ConfirmationModalComponent } from './components/confirmation-modal/confirmation-modal.component';

@NgModule({
  declarations: [AlertComponent, ConfirmationModalComponent],
  imports: [CommonModule, NgbDatepickerModule, NgbToastModule, NgbModalModule],
  exports: [
    NgbDatepickerModule,
    NgbToastModule,
    NgbModalModule,
    AlertComponent,
  ],
  entryComponents: [ConfirmationModalComponent],
})
export class SharedModule {}
