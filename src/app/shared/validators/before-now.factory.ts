import { ValidatorFn, AbstractControl } from '@angular/forms';
import { NgbDate, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

export function beforeNowValidator(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    const date = NgbDate.from(control.value);

    if (date) {
      const now: NgbDateStruct = {
        year: new Date().getFullYear(),
        month: new Date().getMonth() + 1,
        day: new Date().getDate(),
      };

      if (!date.before(now)) {
        return { invalid: true };
      }
    }

    return null;
  };
}
