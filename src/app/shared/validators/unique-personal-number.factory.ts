import { AbstractControl, ValidatorFn } from '@angular/forms';
import { Observable } from 'rxjs';
import { debounceTime, map } from 'rxjs/internal/operators';

import { ApiResponse } from '@core/interfaces/api-response.model';

export function uniquePersonalNumber(
  control: AbstractControl
): Observable<{ [key: string]: any } | null> {
  return this.electronicIdCardService
    .getElectronicIdCardByPersonalNumber(control.value)
    .pipe(
      debounceTime(500),
      map((response: ApiResponse<number>) => {
        if (response.data > -1 && response.data !== this._id) {
          return { isExists: true };
        }

        return null;
      })
    );
}
