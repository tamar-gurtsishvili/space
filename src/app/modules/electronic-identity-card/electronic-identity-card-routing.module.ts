import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ElectronicIdCardContainerComponent } from './containers/electronic-id-card-container/electronic-id-card-container.component';
import { ElectronicIdCardsComponent } from './containers/electronic-id-cards/electronic-id-cards.component';
import { ElectronicIdCardAddComponent } from './containers/electronic-id-card-add/electronic-id-card-add.component';
import { ElectronicIdCardEditComponent } from './containers/electronic-id-card-edit/electronic-id-card-edit.component';

import { ElectronicIdCardsResolver } from './resolvers/electronic-id-cards.resolver';
import { ElectronicIdCardResolver } from './resolvers/electronic-id-card.resolver';

const routes: Routes = [
  {
    path: '',
    component: ElectronicIdCardContainerComponent,
    children: [
      {
        path: '',
        component: ElectronicIdCardsComponent,
        resolve: { electronicIdCards: ElectronicIdCardsResolver },
      },
      {
        path: 'add',
        pathMatch: "full",
        component: ElectronicIdCardAddComponent,
      },
      {
        path: 'edit/:id',
        pathMatch: "full",
        component: ElectronicIdCardEditComponent,
        resolve: { electronicIdCard: ElectronicIdCardResolver },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ElectronicIdentityCardRoutingModule {
  static routeComponents = [
    ElectronicIdCardContainerComponent,
    ElectronicIdCardsComponent,
    ElectronicIdCardAddComponent,
    ElectronicIdCardEditComponent,
  ];

  static routeResolvers = [ElectronicIdCardsResolver, ElectronicIdCardResolver];
}
