import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { SharedModule } from '@shared/shared.module';

import * as formElectronicIdCard from './store/electronic-id-card.reducer';
import { ElectronicIdCardEffects } from './store/electronic-id-card.effects';

import { ElectronicIdentityCardRoutingModule } from './electronic-identity-card-routing.module';

import { ElectronicIdCardsSearchComponent } from './components/electronic-id-cards-search/electronic-id-cards-search.component';
import { ElectronicIdCardTableComponent } from './components/electronic-id-cards-table/electronic-id-cards-table.component.component';
import { ElectronicIdCardFormComponent } from './components/electronic-id-card-form/electronic-id-card-form.component';

@NgModule({
  declarations: [
    ElectronicIdentityCardRoutingModule.routeComponents,
    ElectronicIdCardsSearchComponent,
    ElectronicIdCardTableComponent,
    ElectronicIdCardFormComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    StoreModule.forFeature('electronicIdCard', formElectronicIdCard.reducer),
    EffectsModule.forFeature([ElectronicIdCardEffects]),
    SharedModule,
    ElectronicIdentityCardRoutingModule,
  ],
  providers: [ElectronicIdentityCardRoutingModule.routeResolvers],
})
export class ElectronicIdentityCardModule {}
