
export interface ElectronicIdentityCard {
  id: number;
  firstName: string;
  lastName: string;
  gender: string;
  birthDate: string;
  birthPlace: string;
  citizenship: string;
  personalNumber: string;
  issuingAuthority: string;
  issueDate: string;
  expiryDate: string;
  cardNumber: string;
}
