import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

export interface ElectronicIdCardRawValue {
  id: number;
  firstName: string;
  lastName: string;
  gender: string;
  birthDate: string;
  birthPlace: string;
  citizenship: string;
  personalNumber: string;
  issuingAuthority: string;
  issueDate: string;
  expiryDate: string;
  cardNumber: string;
}

export interface ElectronicIdCardRawFormValue {
  id?: number;
  firstName: string;
  lastName: string;
  gender: string;
  birthDate: NgbDateStruct;
  birthPlace: string;
  citizenship: string;
  personalNumber: string;
  issuingAuthority: string;
  issueDate: NgbDateStruct;
  expiryDate: NgbDateStruct;
  cardNumber: string;
}
