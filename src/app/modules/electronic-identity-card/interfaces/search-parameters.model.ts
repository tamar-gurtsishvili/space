export interface SearchParameters {
  fullName: string;
  personalNumber: string;
  birthPlace: string;
  birthDateFrom: string;
  birthDateTo: string;
  expiryDateFrom: string;
  expiryDateTo: string;
}