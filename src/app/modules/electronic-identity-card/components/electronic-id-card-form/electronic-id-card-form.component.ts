import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import {
  ElectronicIdentityCard,
  ElectronicIdCardRawValue,
  ElectronicIdCardRawFormValue,
} from '../../interfaces';
import { ElectronicIdCardForForm } from '../../utils/electronic-id-card-for-form';
import { ElectronicIdCardForRequest } from '../../utils/electronic-id-card-for-request';
import { ElectronicIdCardService } from '../../services/electronic-id-card.service';

import { beforeNowValidator } from '@shared/validators/before-now.factory';
import { uniquePersonalNumber } from '@shared/validators/unique-personal-number.factory';

@Component({
  selector: 'app-electronic-id-card-form',
  templateUrl: './electronic-id-card-form.component.html',
  styleUrls: ['./electronic-id-card-form.component.scss'],
})
export class ElectronicIdCardFormComponent implements OnInit {
  @Input()
  set data(item: ElectronicIdentityCard) {
    if (item && this.form) {
      const forForm = new ElectronicIdCardForForm(
        item as ElectronicIdCardRawValue
      );
      this.form.patchValue(forForm);
      this._id = item.id;
    }
  }

  @Output() save = new EventEmitter<ElectronicIdentityCard>();

  private _id: number;

  form: FormGroup = this._fb.group({
    id: [''],
    firstName: ['', [Validators.required]],
    lastName: ['', [Validators.required]],
    gender: ['', [Validators.required]],
    birthDate: ['', [Validators.required, beforeNowValidator()]],
    birthPlace: ['', [Validators.required]],
    personalNumber: [
      '',
      [Validators.required, Validators.pattern('^[0-9]{11}$')],
      uniquePersonalNumber.bind(this),
    ],
    citizenship: ['', [Validators.required]],
    issuingAuthority: ['', [Validators.required]],
    issueDate: ['', [Validators.required, beforeNowValidator()]],
    expiryDate: ['', [Validators.required]],
    cardNumber: ['', [Validators.required]],
  });

  constructor(
    private _fb: FormBuilder,
    private electronicIdCardService: ElectronicIdCardService
  ) {}

  ngOnInit() {}

  onClearFrom() {
    this.form.reset();
  }

  onSubmit() {
    if (this.form.valid) {
      const forRequest = new ElectronicIdCardForRequest(
        this.form.value as ElectronicIdCardRawFormValue
      );
      this.save.emit(forRequest);
    }
  }
}
