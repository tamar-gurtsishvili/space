import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElectronicIdCardFormComponent } from './electronic-id-card-form.component';

describe('ElectronicIdCardFormComponent', () => {
  let component: ElectronicIdCardFormComponent;
  let fixture: ComponentFixture<ElectronicIdCardFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElectronicIdCardFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElectronicIdCardFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
