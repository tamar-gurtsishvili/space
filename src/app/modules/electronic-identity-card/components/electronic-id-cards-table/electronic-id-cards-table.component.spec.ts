import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElectronicIdCardTableComponent } from './electronic-id-cards-table.component.component';

describe('ElectronicIdCardTableComponent', () => {
  let component: ElectronicIdCardTableComponent;
  let fixture: ComponentFixture<ElectronicIdCardTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElectronicIdCardTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElectronicIdCardTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
