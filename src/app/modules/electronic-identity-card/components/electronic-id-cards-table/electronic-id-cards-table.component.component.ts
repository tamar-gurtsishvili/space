import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ConfirmationModalComponent } from '@shared/components/confirmation-modal/confirmation-modal.component';

import { ElectronicIdentityCard } from '../../interfaces';

@Component({
  selector: 'app-electronic-id-cards-table',
  templateUrl: './electronic-id-cards-table.component.html',
  styleUrls: ['./electronic-id-cards-table.component.scss'],
})
export class ElectronicIdCardTableComponent implements OnInit {
  @Input() data: ElectronicIdentityCard[];

  @Output() delete = new EventEmitter<number>();

  constructor(private modalService: NgbModal) {}

  ngOnInit() {}

  onDelete(id: number) {
    const modalRef = this.modalService
      .open(ConfirmationModalComponent)
      .result.then(
        (close) => {},
        (dismiss) => {
          this.delete.emit(id);
        }
      );
  }
}
