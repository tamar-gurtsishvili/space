import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { SearchParameters } from '../../interfaces';

@Component({
  selector: 'app-electronic-id-cards-search',
  templateUrl: './electronic-id-cards-search.component.html',
  styleUrls: ['./electronic-id-cards-search.component.scss'],
})
export class ElectronicIdCardsSearchComponent implements OnInit {
  @Output() search = new EventEmitter<SearchParameters>();

  form: FormGroup;

  formSectionClass: string;

  constructor(private _fb: FormBuilder) {}

  ngOnInit() {
    this.form = this._fb.group({
      fullName: [''],
      personalNumber: ['', [Validators.pattern('^[0-9]{11}$')]],
      birthPlace: [''],
      birthDateFrom: [null],
      birthDateTo: [null],
      expiryDateFrom: [null],
      expiryDateTo: [null],
    });
  }

  toggleFormSection() {
    if (!this.formSectionClass || this.formSectionClass === 'collapsed') {
      this.formSectionClass = 'expanded';
    } else {
      this.formSectionClass = 'collapsed';
    }
  }

  onClearFrom() {
    this.form.reset();
    this.search.emit(null);
  }

  onSubmit() {
    this.search.emit(this.form.value);
  }
}
