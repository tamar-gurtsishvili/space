import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElectronicIdCardsSearchComponent } from './electronic-id-cards-search.component';

describe('ElectronicIdCardsSearchComponent', () => {
  let component: ElectronicIdCardsSearchComponent;
  let fixture: ComponentFixture<ElectronicIdCardsSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElectronicIdCardsSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElectronicIdCardsSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
