import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService } from '@core/services/api.service';
import { ApiResponse } from '@core/interfaces';

import { ElectronicIdentityCard, SearchParameters } from '../interfaces';

@Injectable({
  providedIn: 'root',
})
export class ElectronicIdCardService {
  constructor(private _apiService: ApiService) {}

  fetchElectronicIdCards(
    params?: SearchParameters
  ): Observable<ApiResponse<ElectronicIdentityCard[]>> {
    const url = '/electronic-id-cards';

    return this._apiService.get<ElectronicIdentityCard[]>(url, params);
  }

  getElectronicIdCard(
    electronicIdentityCardID: number
  ): Observable<ApiResponse<ElectronicIdentityCard>> {
    const url = '/electronic-id-cards/' + electronicIdentityCardID;

    return this._apiService.get<ElectronicIdentityCard>(url);
  }

  getElectronicIdCardByPersonalNumber(
    personalNamber: string
  ): Observable<ApiResponse<number>> {
    const url = '/electronic-id-cards/personal-number/' + personalNamber;

    return this._apiService.get<number>(url);
  }

  createElectronicIdCard(
    electronicIdentityCard: ElectronicIdentityCard
  ): Observable<ApiResponse<number>> {
    const url = '/electronic-id-cards/create';

    return this._apiService.post<number>(url, electronicIdentityCard);
  }

  updateElectronicIdCard(
    electronicIdentityCard: ElectronicIdentityCard
  ): Observable<ApiResponse<number>> {
    const url = '/electronic-id-card/update';

    return this._apiService.put<number>(url, electronicIdentityCard);
  }

  deleteElectronicIdCard(
    electronicIdentityCardID: number
  ): Observable<ApiResponse<number>> {
    const url = '/electronic-id-cards/delete/' + electronicIdentityCardID;

    return this._apiService.delete<number>(url);
  }
}
