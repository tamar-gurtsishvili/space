import { NgbDateStruct, NgbDate } from '@ng-bootstrap/ng-bootstrap';

import { ElectronicIdCardRawValue } from '../interfaces';

export class ElectronicIdCardForForm {
  id: number;
  firstName: string;
  lastName: string;
  gender: string;
  birthDate: NgbDateStruct;
  birthPlace: string;
  citizenship: string;
  personalNumber: string;
  issuingAuthority: string;
  issueDate: NgbDateStruct;
  expiryDate: NgbDateStruct;
  cardNumber: string;

  constructor(rawValue: ElectronicIdCardRawValue) {
    this.id = rawValue.id;
    this.firstName = rawValue.firstName;
    this.lastName = rawValue.lastName;
    this.gender = rawValue.gender;
    this.birthDate = this._dateStringToStruct(rawValue.birthDate);
    this.birthPlace = rawValue.birthPlace;
    this.citizenship = rawValue.citizenship;
    this.personalNumber = rawValue.personalNumber;
    this.issuingAuthority = rawValue.issuingAuthority;
    this.issueDate = this._dateStringToStruct(rawValue.issueDate);
    this.expiryDate = this._dateStringToStruct(rawValue.expiryDate);
    this.cardNumber = rawValue.cardNumber;
  }

  private _dateStringToStruct(d: string): NgbDateStruct {
    const a = d.split("-"); 
    return new NgbDate(+a[0], +a[1], +a[2]);
  }
}
