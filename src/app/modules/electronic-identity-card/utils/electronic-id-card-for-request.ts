import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

import { ElectronicIdCardRawFormValue } from '../interfaces';

export class ElectronicIdCardForRequest {
  id: number;
  firstName: string;
  lastName: string;
  gender: string;
  birthDate: string;
  birthPlace: string;
  citizenship: string;
  personalNumber: string;
  issuingAuthority: string;
  issueDate: string;
  expiryDate: string;
  cardNumber: string;

  constructor(formValue: ElectronicIdCardRawFormValue) {
    this.firstName = formValue.firstName;
    this.lastName = formValue.lastName;
    this.gender = formValue.gender;
    this.birthDate = this._dateStructToString(formValue.birthDate);
    this.birthPlace = formValue.birthPlace;
    this.citizenship = formValue.citizenship;
    this.personalNumber = formValue.personalNumber;
    this.issuingAuthority = formValue.issuingAuthority;
    this.issueDate = this._dateStructToString(formValue.issueDate);
    this.expiryDate = this._dateStructToString(formValue.expiryDate);
    this.cardNumber = formValue.cardNumber;

    if (formValue.id) {
      this.id = formValue.id;
    }
  }

  private _dateStructToString(d: NgbDateStruct): string {
    return d.year + '-' + d.month + '-' + d.day;
  }
}
