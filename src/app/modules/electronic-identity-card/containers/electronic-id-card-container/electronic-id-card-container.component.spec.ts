import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElectronicIdCardContainerComponent } from './electronic-id-card-container.component';

describe('ElectronicIdCardContainerComponent', () => {
  let component: ElectronicIdCardContainerComponent;
  let fixture: ComponentFixture<ElectronicIdCardContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElectronicIdCardContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElectronicIdCardContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
