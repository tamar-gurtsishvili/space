import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Subscription } from 'rxjs';

import * as fromElectronicIdCard from '../../store';
import * as fromElectronicIdCardReducer from '../../store/electronic-id-card.reducer';

import { Status } from '@core/enums';
import { Alert } from '@core/interfaces';
import { AlertService } from '@core/services/alert.service';

@Component({
  selector: 'app-electronic-id-card-container',
  templateUrl: './electronic-id-card-container.component.html',
  styleUrls: ['./electronic-id-card-container.component.scss'],
})
export class ElectronicIdCardContainerComponent implements OnInit, OnDestroy {
  alertSubscription$: Subscription;

  constructor(
    private _store: Store<fromElectronicIdCardReducer.State>,
    public alertService: AlertService
  ) {}

  ngOnInit(): void {
    this.alertSubscription$ = this._store
      .pipe(select(fromElectronicIdCard.getElectronicIdCardAlert))
      .subscribe((alert: Alert) => {
        if (alert) {
          this._showAlert(alert);
        }
      });
  }

  private _showAlert(alert: Alert) {
    switch (alert.type) {
      case Status.Success:
        this.alertService.showSuccess(alert.message);
        return;

      case Status.Error:
        this.alertService.showError(alert.message);
        return;

      case Status.Warning:
        this.alertService.showWarning(alert.message);
        return;

      default:
        this.alertService.showInfo(alert.message);
    }
  }

  ngOnDestroy() {
    this.alertSubscription$.unsubscribe();
  }
}
