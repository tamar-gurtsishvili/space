import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElectronicIdCardsComponent } from './electronic-id-cards.component';

describe('ElectronicIdentityCardComponent', () => {
  let component: ElectronicIdCardsComponent;
  let fixture: ComponentFixture<ElectronicIdCardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElectronicIdCardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElectronicIdCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
