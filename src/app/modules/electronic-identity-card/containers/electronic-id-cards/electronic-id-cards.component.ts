import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as fromElectronicIdCard from '../../store';
import * as fromElectronicIdCardReducer from '../../store/electronic-id-card.reducer';
import * as fromElectronicIdCardActions from '../../store/electronic-id-card.actions';

import { ElectronicIdentityCard, SearchParameters } from '../../interfaces';

@Component({
  selector: 'app-electronic-id-cards',
  templateUrl: './electronic-id-cards.component.html',
  styleUrls: ['./electronic-id-cards.component.scss'],
})
export class ElectronicIdCardsComponent implements OnInit {
  electronicIdCards$: Observable<ElectronicIdentityCard[]>;

  constructor(private _store: Store<fromElectronicIdCardReducer.State>) {}

  ngOnInit() {
    this.electronicIdCards$ = this._store.pipe(
      select(fromElectronicIdCard.fetchElectronicIdCards)
    );
  }

  onSearch(data: SearchParameters) {
    this._store.dispatch(
      new fromElectronicIdCardActions.ElectronicIdCards(data)
    );
  }

  onElectronicIdCardDelete(id: number) {
    this._store.dispatch(
      new fromElectronicIdCardActions.ElectronicIdCardDelete(id)
    );
  }
}
