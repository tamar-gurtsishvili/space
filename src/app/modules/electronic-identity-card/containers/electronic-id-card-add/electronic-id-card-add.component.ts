import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import * as fromElectronicIdCard from '../../store/electronic-id-card.reducer';
import * as fromElectronicIdCardActions from '../../store/electronic-id-card.actions';

import { ElectronicIdentityCard } from '../../interfaces';

@Component({
  selector: 'app-electronic-id-card-add',
  templateUrl: './electronic-id-card-add.component.html',
  styleUrls: ['./electronic-id-card-add.component.scss'],
})
export class ElectronicIdCardAddComponent implements OnInit {
  constructor(private _store: Store<fromElectronicIdCard.State>) {}

  ngOnInit() {}

  onElectronicIdCreate(data: ElectronicIdentityCard) {
    this._store.dispatch(new fromElectronicIdCardActions.ElectronicIdCardCreate(data));
  }
}
