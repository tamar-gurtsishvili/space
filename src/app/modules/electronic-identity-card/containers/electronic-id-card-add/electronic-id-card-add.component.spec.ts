import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElectronicIdCardAddComponent } from './electronic-id-card-add.component';

describe('ElectronicIdCardAddComponent', () => {
  let component: ElectronicIdCardAddComponent;
  let fixture: ComponentFixture<ElectronicIdCardAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElectronicIdCardAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElectronicIdCardAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
