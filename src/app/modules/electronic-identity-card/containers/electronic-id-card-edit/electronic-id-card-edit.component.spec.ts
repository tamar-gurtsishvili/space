import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElectronicIdCardEditComponent } from './electronic-id-card-edit.component';

describe('ElectronicIdCardEditComponent', () => {
  let component: ElectronicIdCardEditComponent;
  let fixture: ComponentFixture<ElectronicIdCardEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElectronicIdCardEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElectronicIdCardEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
