import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as fromElectronicIdCard from '../../store';
import * as fromElectronicIdCardReducer from '../../store/electronic-id-card.reducer';
import * as fromElectronicIdCardActions from '../../store/electronic-id-card.actions';

import { ElectronicIdentityCard } from '../../interfaces';

@Component({
  selector: 'app-electronic-id-card-edit',
  templateUrl: './electronic-id-card-edit.component.html',
  styleUrls: ['./electronic-id-card-edit.component.scss'],
})
export class ElectronicIdCardEditComponent implements OnInit {
  electronicIdCard$: Observable<ElectronicIdentityCard>;

  constructor(private _store: Store<fromElectronicIdCardReducer.State>) {}

  ngOnInit() {
    this.electronicIdCard$ = this._store.pipe(
      select(fromElectronicIdCard.getElectronicIdCard)
    );
  }

  onElectronicIdUpdate(data: ElectronicIdentityCard) {
    this._store.dispatch(
      new fromElectronicIdCardActions.ElectronicIdCardUpdate(data)
    );
  }
}
