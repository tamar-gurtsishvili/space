import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import * as fromElectronicIdCard from '../store/electronic-id-card.reducer';
import * as fromElectronicIdCardActions from '../store/electronic-id-card.actions';

import { ElectronicIdentityCard } from '../interfaces';

@Injectable({
  providedIn: 'root',
})
export class ElectronicIdCardResolver
  implements Resolve<ElectronicIdentityCard> {
  constructor(private _store: Store<fromElectronicIdCard.State>) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> | Promise<any> | any {
    this._store.dispatch(
      new fromElectronicIdCardActions.ElectronicIdCard(route.params['id'])
    );
  }
}
