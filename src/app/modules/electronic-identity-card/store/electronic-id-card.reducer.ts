import {
  ElectronicIdCardActions,
  ElectronicIdCardActinTypes,
} from './electronic-id-card.actions';

import { Alert } from '@core/interfaces';
import { ElectronicIdentityCard } from '../interfaces';

export interface State {
  electronicIdCards: ElectronicIdentityCard[];
  electronicIdCard: ElectronicIdentityCard;
  electronicIdCardAlert: Alert;
}

const initState: State = {
  electronicIdCards: null,
  electronicIdCard: null,
  electronicIdCardAlert: null,
};

export function reducer(
  state: State = initState,
  action: ElectronicIdCardActions
) {
  switch (action.type) {
    case ElectronicIdCardActinTypes.ELECTRONIC_ID_CARDS_SUCCESS:
      return {
        ...state,
        electronicIdCards: action.payload,
      };

    case ElectronicIdCardActinTypes.ELECTRONIC_ID_CARD_SUCCESS:
      return {
        ...state,
        electronicIdCard: action.payload,
      };

    case ElectronicIdCardActinTypes.ELECTRONIC_ID_CARD_ALERT:
      return {
        ...state,
        electronicIdCardAlert: action.payload,
      };

    default:
      return state;
  }
}
