import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, Effect, createEffect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import {
  mergeMap,
  switchMap,
  map,
  catchError,
  tap,
} from 'rxjs/internal/operators';
import { Action } from '@ngrx/store';

import * as fromElectronicIdCardActions from './electronic-id-card.actions';

import { Status } from '@core/enums';
import { ApiResponse, Alert } from '@core/interfaces';
import { ElectronicIdentityCard } from '../interfaces';
import { ElectronicIdCardService } from '../services/electronic-id-card.service';

@Injectable()
export class ElectronicIdCardEffects {
  @Effect()
  fetchElectronicIdCards$: Observable<Action> = this.actions$.pipe(
    ofType(
      fromElectronicIdCardActions.ElectronicIdCardActinTypes.ELECTRONIC_ID_CARDS
    ),
    switchMap((action: fromElectronicIdCardActions.ElectronicIdCards) => {
      return this._electronicIdCardService
        .fetchElectronicIdCards(action.payload)
        .pipe(
          map(
            (response: ApiResponse<ElectronicIdentityCard[]>) =>
              new fromElectronicIdCardActions.ElectronicIdCardsSuccess(
                response.data
              )
          ),
          catchError(this._errorHandler)
        );
    })
  );

  @Effect()
  getElectronicIdCard$: Observable<Action> = this.actions$.pipe(
    ofType(
      fromElectronicIdCardActions.ElectronicIdCardActinTypes.ELECTRONIC_ID_CARD
    ),
    switchMap((action: fromElectronicIdCardActions.ElectronicIdCard) => {
      return this._electronicIdCardService
        .getElectronicIdCard(action.payload)
        .pipe(
          map(
            (response: ApiResponse<ElectronicIdentityCard>) =>
              new fromElectronicIdCardActions.ElectronicIdCardSuccess(
                response.data
              )
          ),
          catchError(this._errorHandler)
        );
    })
  );

  @Effect()
  createElectronicIdCard$: Observable<Action> = this.actions$.pipe(
    ofType(
      fromElectronicIdCardActions.ElectronicIdCardActinTypes
        .ELECTRONIC_ID_CARD_CREATE
    ),
    switchMap((action: fromElectronicIdCardActions.ElectronicIdCardCreate) => {
      return this._electronicIdCardService
        .createElectronicIdCard(action.payload)
        .pipe(
          map(
            (response: ApiResponse<number>) =>
              new fromElectronicIdCardActions.ElectronicIdCardCreateSuccess()
          ),
          catchError(this._errorHandler)
        );
    })
  );

  @Effect()
  updateElectronicIdCard$: Observable<Action> = this.actions$.pipe(
    ofType(
      fromElectronicIdCardActions.ElectronicIdCardActinTypes
        .ELECTRONIC_ID_CARD_UPDATE
    ),
    switchMap((action: fromElectronicIdCardActions.ElectronicIdCardUpdate) => {
      return this._electronicIdCardService
        .updateElectronicIdCard(action.payload)
        .pipe(
          map(
            (response: ApiResponse<number>) =>
              new fromElectronicIdCardActions.ElectronicIdCardAlert({
                type: Status.Success,
                message: 'Electronic identity card successfully updated',
              })
          ),
          catchError(this._errorHandler)
        );
    })
  );

  @Effect()
  deleteElectronicIdCard$: Observable<Action> = this.actions$.pipe(
    ofType(
      fromElectronicIdCardActions.ElectronicIdCardActinTypes
        .ELECTRONIC_ID_CARD_DELETE
    ),
    switchMap((action: fromElectronicIdCardActions.ElectronicIdCardDelete) => {
      return this._electronicIdCardService
        .deleteElectronicIdCard(action.payload)
        .pipe(
          map(
            (response: ApiResponse<number>) =>
              new fromElectronicIdCardActions.ElectronicIdCards(null)
          ),
          catchError(this._errorHandler)
        );
    })
  );

  @Effect({ dispatch: false })
  electronicIdCardAction = this.actions$.pipe(
    ofType(
      fromElectronicIdCardActions.ElectronicIdCardActinTypes
        .ELECTRONIC_ID_CARD_CREATE_SUCCESS
    ),
    tap(() => {
      this.router.navigate(['/electronic-id-cards']);
    })
  );

  constructor(
    private actions$: Actions,
    private router: Router,
    private _electronicIdCardService: ElectronicIdCardService
  ) {}

  private _errorHandler(error) {
      const msg = error.message ? error.message : error.toString();
      return of(
        new fromElectronicIdCardActions.ElectronicIdCardAlert({
          type: Status.Error,
          message: msg,
        })
      );
  }
}
