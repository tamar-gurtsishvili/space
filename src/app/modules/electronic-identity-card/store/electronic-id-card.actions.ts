import { Action } from '@ngrx/store';

import { Alert } from '@core/interfaces';
import { ElectronicIdentityCard, SearchParameters } from '../interfaces';

export enum ElectronicIdCardActinTypes {
  ELECTRONIC_ID_CARDS = '[ElectronicIdCard] Fetch',
  ELECTRONIC_ID_CARDS_SUCCESS = '[Electronic IdCard] Fetch Success',

  ELECTRONIC_ID_CARD = '[ElectronicIdCard] Get',
  ELECTRONIC_ID_CARD_SUCCESS = '[ElectronicIdCard] Get Success',

  ELECTRONIC_ID_CARD_CREATE = '[ElectronicIdCard] Create',
  ELECTRONIC_ID_CARD_CREATE_SUCCESS = '[ElectronicIdCard] Create Success',

  ELECTRONIC_ID_CARD_UPDATE = '[ElectronicIdCard] Update',

  ELECTRONIC_ID_CARD_DELETE = '[ElectronicIdCard] Delete',

  ELECTRONIC_ID_CARD_ALERT = '[ElectronicIdCard] Alert Status Message',
}

export class ElectronicIdCards implements Action {
  readonly type = ElectronicIdCardActinTypes.ELECTRONIC_ID_CARDS;
  constructor(public payload: SearchParameters) {}
}

export class ElectronicIdCardsSuccess implements Action {
  readonly type = ElectronicIdCardActinTypes.ELECTRONIC_ID_CARDS_SUCCESS;
  constructor(public payload: ElectronicIdentityCard[]) {}
}

export class ElectronicIdCard implements Action {
  readonly type = ElectronicIdCardActinTypes.ELECTRONIC_ID_CARD;
  constructor(public payload: number) {}
}

export class ElectronicIdCardSuccess implements Action {
  readonly type = ElectronicIdCardActinTypes.ELECTRONIC_ID_CARD_SUCCESS;
  constructor(public payload: ElectronicIdentityCard) {}
}

export class ElectronicIdCardCreate implements Action {
  readonly type = ElectronicIdCardActinTypes.ELECTRONIC_ID_CARD_CREATE;
  constructor(public payload: ElectronicIdentityCard) {}
}

export class ElectronicIdCardCreateSuccess implements Action {
  readonly type = ElectronicIdCardActinTypes.ELECTRONIC_ID_CARD_CREATE_SUCCESS;
}

export class ElectronicIdCardUpdate implements Action {
  readonly type = ElectronicIdCardActinTypes.ELECTRONIC_ID_CARD_UPDATE;
  constructor(public payload: ElectronicIdentityCard) {}
}

export class ElectronicIdCardDelete implements Action {
  readonly type = ElectronicIdCardActinTypes.ELECTRONIC_ID_CARD_DELETE;
  constructor(public payload: number) {}
}

export class ElectronicIdCardAlert implements Action {
  readonly type = ElectronicIdCardActinTypes.ELECTRONIC_ID_CARD_ALERT;
  constructor(public payload: Alert) {}
}

export type ElectronicIdCardActions =
  | ElectronicIdCards
  | ElectronicIdCardsSuccess
  | ElectronicIdCard
  | ElectronicIdCardSuccess
  | ElectronicIdCardCreate
  | ElectronicIdCardCreateSuccess
  | ElectronicIdCardUpdate
  | ElectronicIdCardDelete
  | ElectronicIdCardAlert;
