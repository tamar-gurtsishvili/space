import { createFeatureSelector, createSelector } from '@ngrx/store';

import { State } from './electronic-id-card.reducer'

const getState = createFeatureSelector<State>('electronicIdCard');

export const fetchElectronicIdCards = createSelector(
  getState,
  (state) => state.electronicIdCards
);

export const getElectronicIdCard = createSelector(
  getState,
  (state) => state.electronicIdCard
);

export const getElectronicIdCardAlert = createSelector(
  getState,
  (state) => state.electronicIdCardAlert
);
