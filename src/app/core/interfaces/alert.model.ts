import { Status } from '@core/enums';

export interface Alert {
    type: Status;
    message: string
}
