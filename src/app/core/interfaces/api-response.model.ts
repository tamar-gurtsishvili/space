import { Status } from '@core/enums';

export interface ApiResponse<T> {
    data: T;
    status: Status
}
