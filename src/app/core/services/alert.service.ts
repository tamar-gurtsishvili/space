import { Injectable, TemplateRef } from '@angular/core';

@Injectable()
export class AlertService {
  toasts: any[] = [];

  remove(toast) {
    this.toasts = this.toasts.filter((t) => t !== toast);
  }

  showError(message: string) {
    this._show(message, {
      classname: 'bg-danger text-light',
      delay: 5000,
    });
  }

  showSuccess(message: string) {
    this._show(message, {
      classname: 'bg-success text-light',
      delay: 5000,
    });
  }

  showWarning(message: string) {
    this._show(message, {
      classname: 'bg-info text-light',
      delay: 5000,
    });
  }

  showInfo(message: string) {
    this._show(message, {
      delay: 5000,
    });
  }

  private _show(textOrTpl: string | TemplateRef<any>, options: any = {}) {
    this.toasts.push({ textOrTpl, ...options });
  }
}
