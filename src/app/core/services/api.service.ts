import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/internal/operators';

import { environment } from '../../../environments/environment';

import { ApiResponse } from '@core/interfaces';

@Injectable()
export class ApiService {
  private _apiUrl = environment.apiUrl;

  constructor(private _http: HttpClient) {}

  get<T>(url: string, queryParams?: any): Observable<ApiResponse<T>> {
    const fullPath = this._apiUrl + url;

    const httpParams = this._getHttpParams(queryParams);

    return this._http
      .get<ApiResponse<T>>(fullPath, { params: httpParams })
      .pipe(
        catchError((error) => {
          console.log(error);
          throw error;
        })
      );
  }

  post<T>(url: string, params?: any): Observable<ApiResponse<T>> {
    const fullPath = this._apiUrl + url;

    return this._http.post<ApiResponse<T>>(fullPath, params).pipe(
      catchError((error) => {
        console.log(error);
        throw error;
      })
    );
  }

  put<T>(url: string, params?: any): Observable<ApiResponse<T>> {
    const fullPath = this._apiUrl + url;

    return this._http.put<ApiResponse<T>>(fullPath, params).pipe(
      catchError((error) => {
        console.log(error);
        throw error;
      })
    );
  }

  delete<T>(url: string): Observable<ApiResponse<T>> {
    const fullPath = this._apiUrl + url;

    return this._http.delete<ApiResponse<T>>(fullPath).pipe(
      catchError((error) => {
        console.log(error);
        throw error;
      })
    );
  }

  private _getHttpParams(params: any) {
    let httpParams = new HttpParams();

    for (const key in params) {
      const element = params[key];

      if (element) {
        httpParams = httpParams.append(key, element);
      }
    }

    return httpParams;
  }
}
