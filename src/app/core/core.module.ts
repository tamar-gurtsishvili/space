import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ApiService } from './services/api.service';
import { AlertService } from './services/alert.service';

@NgModule({
  declarations: [],
  imports: [CommonModule],
  providers: [ApiService, AlertService],
})
export class CoreModule {}
