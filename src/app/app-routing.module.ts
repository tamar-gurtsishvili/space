import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'electronic-id-cards',
    loadChildren: () =>
      import('./modules/electronic-identity-card/electronic-identity-card.module').then(
        (m) => m.ElectronicIdentityCardModule
      ),
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'electronic-id-cards'
  },
  {
    path: '**',
    redirectTo: 'electronic-id-cards'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
